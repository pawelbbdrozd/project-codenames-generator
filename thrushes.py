import csv
import random
from dataclasses import astuple, dataclass, field, fields
from typing import Any, List, TextIO

FILE_NAME = "thrushes.csv"


@dataclass(frozen=True)
class ThrushSpecies:
    en_name: str
    lat_name: str
    generic_name: str = field(default="")
    specific_epithet: str = field(default="")
    en_project_name: str = field(default="")
    lat_project_name: str = field(default="")

    def __post_init__(self):
        if not self.generic_name or not self.specific_epithet:
            generic, epithet = self.lat_name.split()
            self.__setattr__("generic_name", generic)
            self.__setattr__("specific_epithet", epithet)

        if not self.en_project_name:
            self.__setattr__(
                "en_project_name", self._create_en_project_name(self.en_name)
            )

        if not self.lat_project_name:
            self.__setattr__(
                "lat_project_name", self._create_lat_project_name(self.lat_name)
            )

    def _create_en_project_name(self, name: str) -> str:
        return (
            name.lower()
            .replace("-thrush", "_thrush")
            .replace(" ", "_")
            .replace("’", "")
            .replace("-", "")
        )

    def _create_lat_project_name(self, name: str) -> str:
        return name.lower().replace(" ", "_")

    @staticmethod
    def get_fields_list() -> List[str]:
        return list(map(lambda x: x.name, fields(ThrushSpecies)))


class Thrushes:
    __data: List[ThrushSpecies]
    __file: str

    def __init__(self, filename=FILE_NAME):
        self.__file = filename
        self.load()

    @property
    def data(self):
        return self.__data

    def load(self):
        """Load data from csv file."""
        self.__data = self._load_from_csv(self.__file)

    def _load_from_csv(self, filename) -> List[ThrushSpecies]:
        with open(filename) as thrushes_csv:
            reader = Thrushes.Reader(thrushes_csv)
            return reader.read()

    def save(self):
        """Save data to csv file"""
        self._save_to_csv(self.__file, self.__data)

    def _save_to_csv(self, filename: str, data: List[ThrushSpecies]):
        with open(filename, "w") as thrushes_csv:
            writer = Thrushes.Writer(thrushes_csv)
            writer.write(data)

    def get_random(self) -> ThrushSpecies:
        """Return randomly selected `ThrushSpecies`.

        Returns:
            A `ThrushSpecies` instance.
        """
        return random.choice(self.__data)

    class Reader:
        """Reader for CSV file."""

        _reader: csv.reader

        def __init__(self, file: TextIO):
            self._reader = csv.reader(file)

        def read(self) -> List[ThrushSpecies]:
            """Read file and return parsed data.

            Returns:
                List of `ThrushSpecies`
            """
            return [
                ThrushSpecies(*row)
                for row in self._reader
                if not self._should_be_excluded(row)
            ]

        def _should_be_excluded(self, row: List[Any]) -> bool:
            return self._is_header(row) or self._is_empty(row)

        def _is_empty(self, row: List[Any]) -> bool:
            return not all(row[:2])

        def _is_header(self, row: List[Any]) -> bool:
            fields_list = ThrushSpecies.get_fields_list()
            return len(row) == len(fields_list) and row[0] in fields_list

    class Writer:
        """Writer for CSV file."""

        _writer: csv.writer

        def __init__(self, file: TextIO):
            self._writer = csv.writer(file)

        def write(self, data: List[ThrushSpecies]):
            """Write data to CSV file with header in first line.

            Args:
                data: Data to save.
            """
            self._write_header()
            self._write_data(data)

        def _write_header(self):
            self._writer.writerow(ThrushSpecies.get_fields_list())

        def _write_data(self, data: List[ThrushSpecies]):
            for row in data:
                self._writer.writerow(astuple(row))
